package com.example.televiandroid.presentation.scene.movieList

interface MovieListContract {

    interface View {
        fun showLoading()
        fun hideLoading()
        fun showList(movieList: List<Movie>)
        fun showGetMovieListError()
    }

    interface Presenter {
        fun getMovieList()
    }

}