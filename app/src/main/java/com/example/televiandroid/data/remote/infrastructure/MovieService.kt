package com.example.televiandroid.data.remote.infrastructure

import com.example.televiandroid.presentation.scene.movieDetail.MovieDetail
import com.example.televiandroid.presentation.scene.movieList.Movie
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url

interface MovieService {
    @GET("movies")
    fun fetchMovieList() : Call<List<Movie>>

    @GET("movies/{id}")
    fun fetchMovieDetail(@Path("id")id: String): Call<MovieDetail>
}