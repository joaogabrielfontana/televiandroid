package com.example.televiandroid.presentation.scene.movieDetail

import com.google.gson.annotations.SerializedName

data class MovieDetail(
    val title: String,
    @SerializedName("poster_url")
    val posterUrl: String,
    @SerializedName("vote_average")
    val voteAverage: String,
    val genres: List<String>,
    @SerializedName("overview")
    val description: String,
)