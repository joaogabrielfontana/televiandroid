import com.example.televiandroid.data.remote.infrastructure.RetrofitInitializer
import com.example.televiandroid.presentation.scene.movieDetail.MovieDetail
import com.example.televiandroid.presentation.scene.movieList.Movie
import retrofit2.Call

class MovieRepository {

    private val movieService = RetrofitInitializer().createMovieService()

    fun fetchMovieList(): Call<List<Movie>> {
        return movieService.fetchMovieList()
    }

    fun fetchMovieDetail(movieId: String): Call<MovieDetail> {
        return movieService.fetchMovieDetail(movieId)
    }
}