package com.example.televiandroid.presentation.scene.movieDetail

interface MovieDetailContract {

    interface View {
        fun showMovieDetail(movieDetail: MovieDetail)
        fun showLoading()
        fun hideLoading()
        fun showGetMovieDetailError()
    }

    interface Presenter {
        fun getMovieDetail(movieId: String)
    }

}