package com.example.televiandroid.presentation.scene.movieList

import android.content.Context
import android.view.View
import com.bumptech.glide.Glide
import com.example.televiandroid.R
import com.example.televiandroid.databinding.MovieListItemBinding
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.viewbinding.BindableItem

class MovieListAdapter(private val context: Context, val clickListener: ((index: Int) -> Unit)) :
    GroupAdapter<GroupieViewHolder>() {

    fun setData(movieList: List<Movie>) {
        clear()
        movieList.forEach { add(MovieItem(it)) }
    }

    inner class MovieItem(private val movie: Movie) :
        BindableItem<MovieListItemBinding>() {

        override fun getLayout(): Int {
            return R.layout.movie_list_item
        }

        override fun bind(viewBinding: MovieListItemBinding, position: Int) {
            viewBinding.nameTextView.text = movie.title
            viewBinding.voteAverageTextView.text = movie.voteAverage
            viewBinding.releaseDateTextView.text = movie.releaseDate
            Glide.with(context).load(movie.imageUrl).placeholder(R.drawable.ic_movie)
                .into(viewBinding.moviePosterImageView)
            viewBinding.root.setOnClickListener {
                clickListener(position)
            }
        }

        override fun initializeViewBinding(view: View): MovieListItemBinding {
            return MovieListItemBinding.bind(view)
        }
    }
}