package com.example.televiandroid.presentation.scene.movieDetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.example.televiandroid.R
import com.example.televiandroid.databinding.ActivityMovieDetailBinding

class MovieDetailActivity : AppCompatActivity(), MovieDetailContract.View {
    companion object {
        private const val movieIdKey: String = "MOVIE_ID"
    }
    private lateinit var binding: ActivityMovieDetailBinding
    private var movieDetailPresenter: MovieDetailPresenter = MovieDetailPresenter(this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var movieId: String? = intent.getStringExtra(movieIdKey)
        binding = ActivityMovieDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        if(movieId != null) {
            movieDetailPresenter.getMovieDetail(movieId)
        }
    }

    override fun showMovieDetail(movieDetail: MovieDetail) {
        binding.movieTitleTextView.text = movieDetail.title
        binding.movieDescriptionTextView.text = movieDetail.description
        binding.movieGenreTextView.text = movieDetail.genres.joinToString(" ")
        binding.movieVoteAverageTextView.text = movieDetail.voteAverage
        Glide.with(this).load(movieDetail.posterUrl).placeholder(R.drawable.ic_movie).into(binding.moviePosterImageView)
    }

    override fun showLoading() {
        binding.loadingView.isVisible = true
    }

    override fun hideLoading() {
        binding.loadingView.isVisible = false
    }

    override fun showGetMovieDetailError() {
        binding.errorView.isVisible = true
    }
}