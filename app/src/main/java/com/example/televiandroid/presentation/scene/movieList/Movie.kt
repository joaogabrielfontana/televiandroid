package com.example.televiandroid.presentation.scene.movieList

import com.google.gson.annotations.SerializedName

data class Movie(
    val id: String,
    val title: String,
    @SerializedName("poster_url")
    val imageUrl: String,
    @SerializedName("release_date")
    val releaseDate: String,
    @SerializedName("vote_average")
    val voteAverage: String
)