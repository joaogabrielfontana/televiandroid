package com.example.televiandroid.presentation.scene.movieDetail

import MovieRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieDetailPresenter(val view: MovieDetailContract.View) : MovieDetailContract.Presenter {
    private val movieRepository: MovieRepository = MovieRepository()

    override fun getMovieDetail(movieId: String) {
        view.showLoading()
        movieRepository.fetchMovieDetail(movieId).enqueue(object : Callback<MovieDetail> {
            override fun onResponse(call: Call<MovieDetail>, response: Response<MovieDetail>) {
                if (response.body() != null) {
                    view.showMovieDetail(response.body()!!)
                    view.hideLoading()
                }
            }
            override fun onFailure(call: Call<MovieDetail>, t: Throwable) {
                view.showGetMovieDetailError()
                view.hideLoading()
            }
        })
    }
}