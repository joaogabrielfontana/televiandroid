package com.example.televiandroid.presentation.scene.movieList

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.televiandroid.databinding.ActivityMovieListBinding
import com.example.televiandroid.presentation.scene.movieDetail.MovieDetailActivity


class MovieListActivity : AppCompatActivity(), MovieListContract.View {
    companion object {
       private const val movieIdKey: String = "MOVIE_ID"
    }

    private lateinit var binding: ActivityMovieListBinding
    val presenter: MovieListContract.Presenter = MovieListPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieListBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        presenter.getMovieList()
    }

    private fun onMovieClicked(movieId: String) {
        val intent = Intent(this, MovieDetailActivity::class.java)
        intent.putExtra(movieIdKey, movieId)
        startActivity(intent)
    }

    override fun showLoading() {
        binding.loadingView.isVisible = true
    }

    override fun hideLoading() {
        binding.loadingView.isVisible = false
    }

    override fun showList(movieList: List<Movie>) {
        val adapter = MovieListAdapter(this) {
            position -> onMovieClicked(movieList[position].id)
        }
        binding.movieListRecyclerView.adapter = adapter
        binding.movieListRecyclerView.layoutManager =
            LinearLayoutManager(this)
        adapter.setData(movieList)
    }

    override fun showGetMovieListError() {
        binding.errorView.isVisible = true
    }
}