package com.example.televiandroid.presentation.scene.movieList

import MovieRepository

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieListPresenter(val view: MovieListContract.View): MovieListContract.Presenter {

    private val movieRepository: MovieRepository = MovieRepository()

    override fun getMovieList() {
        view.showLoading()
        movieRepository.fetchMovieList().enqueue(object : Callback<List<Movie>> {
            override fun onResponse(call: Call<List<Movie>>, response: Response<List<Movie>>) {
                if(response.body() != null) {
                    view.showList(response.body()!!)
                    view.hideLoading()
                }
            }

            override fun onFailure(call: Call<List<Movie>>, t: Throwable) {
                view.showGetMovieListError()
                view.hideLoading()
            }
        })
    }
}