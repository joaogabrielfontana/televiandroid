package com.example.televiandroid.data.remote.infrastructure

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class RetrofitInitializer {

    companion object {
        private const val BASE_URL = "https://desafio-mobile.nyc3.digitaloceanspaces.com/"
    }

    private val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun createMovieService(): MovieService = retrofit.create(MovieService::class.java)
}